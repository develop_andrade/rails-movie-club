class CreateMovies < ActiveRecord::Migration[5.0]
  def change
    create_table :movies do |t|
        t.string :titulo
        t.text :descripcion
        t.string :productor
        t.string :poster
        t.string :genero
        t.string :precio
        t.integer :calificacion, default: 1
        t.timestamps
    end
  end
end
