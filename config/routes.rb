Rails.application.routes.draw do
  resources :movies
  get 'static_pages/home'

  get 'static_pages/movies'

    root 'application#hello'
    get  '/registro',  to: 'users#new'
    post  '/users',  to: 'users#store'
    get    '/login',   to: 'sessions#new'
    post   '/login',   to: 'sessions#create'
    delete '/logout',  to: 'sessions#destroy'

    post  '/rent_a_movie',  to: 'movies#make_a_rent'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
