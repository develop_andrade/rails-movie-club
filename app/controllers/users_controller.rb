class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def store
    @user = User.new(user_params) 
    if @user.save
        flash[:success] = "Bienvenido a Rails Videoclub!"
        log_in @user
        redirect_to movies_path
    else
        render 'new'
    end
  end

  private
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
end
