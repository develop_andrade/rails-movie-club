class Movie < ApplicationRecord
    mount_uploader :poster, PosterUploader
    validates :titulo, presence: true
    validates :descripcion, presence: true
    validates :productor, presence: true
    validates :genero, presence: true
    validates :precio, presence: true
end
